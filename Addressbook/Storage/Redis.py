import redis
from Base import Base
from Addressbook import Entry

class Redis(Base):

    def __init__(self, prefix = 'ginger.addressbook.', host = 'localhost', port = 6379):
        self.r = redis.StrictRedis(host = host, port = port)
        self.prefix = str(prefix)

    def read(self):
        keys = self.r.smembers(self._('set'))
        result = []
        for key in keys:
            result.append(Entry(**self.r.hgetall(key)))
        return result

    def write(self, data):
        keys = []
        pipe = self.r.pipeline()
        for index, record in enumerate(data):
            key = self._(index)
            pipe.delete(key)
            pipe.hmset(key, record._asdict())
            keys.append(key)
        pipe.delete(self._('set'))
        if len(keys) > 0:
            pipe.sadd(self._('set'), *keys)
        pipe.execute()

    def _(self, name):
        return self.prefix + str(name)

