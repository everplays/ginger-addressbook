import yaml
import os
from Base import Base
from Addressbook import Entry

class FileSystem(Base):

    def __init__(self, file_path):
        self.file_path = file_path

    def read(self):
        return map(lambda data: Entry(**data), yaml.load(self.read_file()))

    def write(self, data):
        self.write_file(yaml.dump(map(lambda entry: entry._asdict(), data)))

    def read_file(self):
        if not os.path.exists(self.file_path):
            return '[]'
        with open(self.file_path, 'r+') as f:
            return f.read()

    def write_file(self, content):
        with open(self.file_path, 'w+') as f:
            f.write(content)

