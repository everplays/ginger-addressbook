from abc import ABCMeta, abstractmethod

class Base:

    __metaclass__ = ABCMeta

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def write(self, data):
        pass

