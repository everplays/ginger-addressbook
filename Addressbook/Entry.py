import sys
from collections import namedtuple

entryTuple = namedtuple(
    'Entry',
    [
        'first_name',
        'last_name',
        'addresses',
        'emails',
        'phones',
        'groups'
    ]
)

class Entry(entryTuple):

    def match(self, criteria):
        keys_to_check = set(criteria.keys()).intersection(self.__class__._fields)
        if len(keys_to_check) == 0:
            return False
        result = True
        dictionary = self._asdict()
        for key in keys_to_check:
            value = dictionary[key]
            if isinstance(value, list):
                result &= any(criteria[key].lower() in s.lower() for s in value)
            else:
                result &= (criteria[key].lower() in str(value).lower())
        return result

    def isInGroup(self, group):
        # not using match to have more explicit check.
        return (group in self.groups)

sys.modules[__name__] = Entry
