import Entry
import Storage

class Addressbook:

    def __init__(self, storage):
        self.storage = storage
        self.__records = storage.read()

    def add(self, entry):
        self.__records.append(entry)

    def save(self):
        self.storage.write(self.__records)

    def records(self):
        return tuple(self.__records)

    def find_by(self, criteria):
        return [entry for entry in self.__records if entry.match(criteria)]

Addressbook.Entry = Entry
Addressbook.Storage = Storage
