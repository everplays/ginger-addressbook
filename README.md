Ginger Addressbook
------------------

Ginger addressbook is a simple immutable addressbook implementation which
allows you to implement any storage backend you want. Currently, it comes with
built-in `FileSystem` and `Redis` storages.

Usage
-----

```python
# import the package
import Addressbook

# make multiple addressbooks
a1 = Addressbook(Addressbook.Storage.Redis(prefix='a1'))
a2 = Addressbook(Addressbook.Storage.Redis(prefix='a2'))

# add entries to addressbook
a1.add(
    Addressbook.Entry(**{
        'first_name': 'Behrooz',
        'last_name': 'Shabani',
        'addresses': [
            'Enschede, the Netherlands',
        ],
        'emails': [
            'behrooz@example.com'
        ],
        'phones': [ '123456' ],
        'groups': [ 'important', 'enschede' ]
    })
)

john = Addressbook.Entry(
    first_name = 'John',
    last_name = 'Smith',
    addresses = [
        'nowhere',
    ],
    emails = [
        'john@example.com'
    ],
    phones = [ '456789' ],
    groups = [ 'imaginary' ]
)
a2.add(john)

a1.save()
a2.save()

# load the addressbooks again
a1 = Addressbook(Addressbook.Storage.Redis(prefix='a1'))
a2 = Addressbook(Addressbook.Storage.Redis(prefix='a2'))

# find entries by different properties:
a2.find_by({ 'first_name': 'John' })
a2.find_by({ 'groups': 'imaginary' })
a2.find_by({ 'addresses': 'nowhere' })
a2.find_by({ 'emails': 'john' })

# get all records of addressbook:
a2.records() # returns a tuple of all records

# also each entry has `match` and `isInGroup` methods:
john.isInGroup('imaginary') # True
john.isInGroup('important') # False
john.match({ 'first_name': 'John' }) # True
```
