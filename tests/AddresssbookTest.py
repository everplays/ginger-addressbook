import unittest
import os.path
import sys
import tempfile

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import Addressbook

class AddressbookTest(unittest.TestCase):

    def entry(self, **p):
        default = {
            'first_name': 'Behrooz',
            'last_name': 'Shabani',
            'addresses': [
                'Enschede, the Netherlands',
            ],
            'emails': [
                'foo@example.com'
            ],
            'phones': [ '123456' ],
            'groups': [ 'important', 'enschede' ],
        }
        return Addressbook.Entry(**dict(default.items() + p.items()))

    def test_entry(self):
        record = self.entry()
        self.assertTrue(record.match({ 'first_name': 'behrooz' }))
        self.assertTrue(record.match({ 'first_name': 'hrooz' }))
        self.assertTrue(record.match({ 'addresses': 'enschede' }))
        self.assertTrue(record.match({ 'emails': 'foo' }))
        self.assertFalse(record.match({ 'first_name': 'foo' }))
        self.assertFalse(record.isInGroup('foobar'))
        self.assertTrue(record.isInGroup('important'))

    def test_redis_storage(self):
        storage = Addressbook.Storage.Redis()
        self.assertIsInstance(storage.read(), list)
        storage.write([self.entry()])
        self.assertEqual(1, len(storage.read()))
        self.assertIsInstance(storage.read()[0], Addressbook.Entry)

    def test_file_system_storage(self):
        storage = Addressbook.Storage.FileSystem(tempfile.NamedTemporaryFile().name)
        self.assertIsInstance(storage.read(), list)
        storage.write([self.entry()])
        self.assertEqual(1, len(storage.read()))
        self.assertIsInstance(storage.read()[0], Addressbook.Entry)

    def test_addressbook(self):
        storage = Addressbook.Storage.FileSystem(tempfile.NamedTemporaryFile().name)
        addressbook = Addressbook(storage)
        addressbook.add(self.entry())
        self.assertEqual(1, len(addressbook.records()))
        addressbook.save()
        storage.read()
        self.assertEqual(1, len(storage.read()))

    def test_base_storage(self):
        self.assertRaises(TypeError, Addressbook.Storage.Base)

if __name__ == '__main__':
    unittest.main()
